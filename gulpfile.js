var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.assetsPath = 'assets/';

elixir(function (mix) {
    // mix.copy('lib/bootstrap/less', 'assets/less/bootstrap');
    // mix.copy('lib/bootstrap/fonts', 'public/fonts');
    // mix.copy('lib/font-awesome/less', 'assets/less/font-awesome');
    // mix.copy('lib/font-awesome/fonts', 'public/fonts');
    // mix.browserify('app.js', null, 'assets/js');
    //mix.browserify('events.js', 'public/js/eventPage.js', 'assets/js');
    mix.sass(['common.scss'],'dist/css/common.css');
    // combine all plugins and scripts
    // first parameter is the array of files to be combined
    // second parameter is the output file location
    // third parameter is the folder where the script is located
    mix.scripts([
    	'jquery/dist/jquery.min.js',
    	'bootstrap/dist/js/bootstrap.min.js'
    	],'dist/js/plugins.min.js','lib')
       .scripts(['app.js'], 'dist/js/app.js');
});